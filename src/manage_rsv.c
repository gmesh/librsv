/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: librsv
 * FILE NAME: manage_rsv.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Nicolas GALLOIS, Baptiste LABARTHE
 *
 * LIBRARY BRIEF DESCRIPTION: Simulation of a reservoir 0.5D.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the librsv Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        manage_rsv.c
 * @brief       Functions dealing with attributes of the s_reservoir_rsv structure
 * @author      Nicolas FLIPO, Nicolas GALLOIS, Baptiste LABARTHE
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "reservoir.h"

/**
 * @brief Frees a reservoir and its content
 * @return NULL if successful
 */
s_reservoir_rsv *RSV_free_rsv(s_reservoir_rsv *prsv) {
  int i;

  if (prsv->p_layer != NULL) {
    for (i = 0; i < prsv->nb_layer; i++) {
      if (prsv->p_layer[i] != NULL) {
        free(prsv->p_layer[i]);
      }
      prsv->p_layer[i] = NULL;
    }
  }
  free(prsv);
  prsv = NULL;
  return prsv;
}

/**
 * @brief Links two reservoirs.
 */
s_reservoir_rsv *RSV_chain_rsv_fwd(s_reservoir_rsv *prsv, s_reservoir_rsv *prsv2) {
  prsv->next = prsv2;
  prsv2->prev = prsv;
  return prsv2;
}

/**
 * @brief Links two reservoirs.
 */
s_reservoir_rsv *RSV_secured_chain_rsv_fwd(s_reservoir_rsv *prsv, s_reservoir_rsv *prsv2) {
  if (prsv2 != NULL) {
    prsv = RSV_chain_rsv_fwd(prsv, prsv2);
  } else {
    prsv = prsv2;
  }
  return prsv;
}

/**
 * @brief Allocates and fills a reservoir structure with default init parameters.
 * `Height`, `Qout`, and `bond_water` are the values to be provided for the reservoir.
 */
s_reservoir_rsv *RSV_create_reservoir(double height, double Q_out, double bond_water) {

  s_reservoir_rsv *prsv;

  prsv = new_reservoir();
  bzero((char *)prsv, sizeof(s_reservoir_rsv));

  prsv->bond_water = bond_water;
  prsv->c_Qout = Q_out;
  prsv->height = height;      // NF 26/07/2003
  prsv->p_layer = NULL;       // NG 04/08/2020
  prsv->nb_layer = 0;         // NG 04/08/2020
  prsv->layer_buffersize = 0; // NG 14/04/2023. Will be assigned to BUFFER_NB_LAYER_RSV when **p_layer are initialized for transport.
  prsv->z = 0.;               // NG 22/11/2024

  return (prsv);
}

/**
 * @brief Calculates, in a simplified way (no internal reservoir layers), the reservoir water balance
 * and returns the new water height in the reservoir.
 * `pwat_bal` is the incoming hydrological balance (input water height), which will equal the water balance
 * for the next reservoir at the end of the function. `qmax` is the maximum water height of the reservoir,
 * `Qout` is the drainage coefficient, and `bond_water` represents the associated water depth.
 * `layer` specifies the type of calculation, while `pr` is a pointer to the reservoir.
 * @internal NG 05/12/2023: `io_rerouting` defines how the reservoir overflow is handled:
 * - If set to `off` (= NO), the overflow is added to the reservoir's outflow (common default behavior).
 * - If set to `on` (= YES), the overflow is saved and can subsequently be added to another reservoir (`res_ovf`). @endinternal
 */
double RSV_transit_simple(s_wat_bal_rsv *pwat_bal, double qmax, double Qout, double bond_water, int layer, s_reservoir_rsv *pr, int io_rerouting, int res_ovf, FILE *fpout) {

  double bzns = 0, lev_reservoir = 0., q_surverse = 0., cq = 0.;

  if (pr != NULL) {
    lev_reservoir = pr->z;
  } else {
    lev_reservoir = pwat_bal->r[layer];
  }

  if (layer == RSV_RUIS) {
    lev_reservoir += (pwat_bal->q[layer] + pwat_bal->ovf[res_ovf]);
  } else
    lev_reservoir += (pwat_bal->q[layer]);

  q_surverse = TS_max(0, (lev_reservoir - qmax));
  lev_reservoir -= q_surverse;
  // LP_printf(fpout,"%s Q_overflow : %f",RSV_name_res(layer),q_surverse); //BL to debug

  if (lev_reservoir > bond_water) {
    cq = Qout;
  }

  if (io_rerouting == YES) {
    pwat_bal->q[layer] = cq * (lev_reservoir - bond_water);
    pwat_bal->ovf[layer] = q_surverse;
    pwat_bal->r[layer] = (1 - cq) * (lev_reservoir) + bond_water * cq;
  } else { // Common case
    pwat_bal->q[layer] = cq * (lev_reservoir - bond_water) + q_surverse;
    pwat_bal->r[layer] = (1 - cq) * (lev_reservoir) + bond_water * cq;
  }

  bzns = pwat_bal->r[layer];
  if (pr != NULL) {
    pr->z = pwat_bal->r[layer];
  }
  // LP_printf(fpout," lev_reservoir %f\n", pwat_bal->r[layer]); //BL to debug
  return bzns;
}

/**
 * @brief Computes water and solute transit through a single layered bucket.
 * Updates the structure of the reservoir.
 * @internal 'res_out' targets the transit WATBAL reservoir.
 */
void RSV_transit_layered_reservoir(int idres, int id_zns, int nb_species, s_reservoir_rsv *prsv, s_wat_bal_rsv *pwatbal, int res_out, FILE *fpout) {

  int i, j, k, p;
  double z_init;          // Reservoir water height before a new inflow (mm)
  double q_surverse = 0.; // Outflow water height due to overflow (mm)
  double q_grav = 0.;     // Outflow water height at the reservoir bottom (mm)
  double *f_init;         // Backup array for incoming matter flux (g/m2) in the reservoir.
  double *fout;           // Total drained matter flux from the reservoir, for each specie (g.m-2.mm !)
  double qout = 0.;       // Total drained water height (mm)
  double *cout;           // Concentrations of drained water height, for each specie (g.m-3)
  double q_remain = 0.;   // Remaining water height to be drained in a given layer (mm)
  int count_layer = 0;    // Number of layers which take part (totally or partially) in reservoir drainage. (-)
  double sum_h_layer = 0.;

  // LP_printf(fpout,"Id ZNS %d IDres %d\n",id_zns,idres); // NG check

  fout = (double *)calloc(nb_species, sizeof(double));
  cout = (double *)calloc(nb_species, sizeof(double));
  f_init = (double *)calloc(nb_species, sizeof(double));

  if (fout == NULL || cout == NULL || f_init == NULL) {
    LP_error(fpout, "librsv%4.2f : Error in file %s, function %s at line %d : Memory allocation failure.\n", VERSION_RSV, __FILE__, __func__, __LINE__);
  }

  z_init = prsv->z;

  // Backup for reservoir matter storage calculation
  for (p = 0; p < nb_species; p++)
    f_init[p] = pwatbal->trflux[p][res_out];

  // Checking if **p_layers needs to be extended with BUFFER_NB_LAYER_RSV new layers
  if (prsv->nb_layer >= prsv->layer_buffersize) {
    RSV_init_new_chunk_layers(prsv, nb_species, fpout);
  }

  /* Adding the inflows either as a new reservoir layer or
     dissolution (totally or partially) into storage capacity bottom layer.
     Only launched in case of non-null water input. */
  if (pwatbal->q[res_out] > EPS_RSV) {
    if (prsv->z > prsv->bond_water)
      RSV_fill_top_layer(nb_species, prsv, pwatbal, res_out);
    else
      RSV_dissolve_layer_into_storage_cap(nb_species, prsv, pwatbal, res_out, fpout);
  }

  if (prsv->z > prsv->bond_water) // Reservoir drainage case
  {
    if (prsv->z > prsv->height) // Overflow
    {
      q_surverse = prsv->z - prsv->height; // mm
      prsv->z = prsv->height;
      for (p = 0; p < nb_species; p++)
        fout[p] += (pwatbal->trvar[p][res_out] * q_surverse); // overflow concentration = cinf of current time step

      // Updating the overflow layer water height
      prsv->p_layer[prsv->nb_layer - 1]->height -= q_surverse;
    }

    // Drained water height
    q_grav = (prsv->z - prsv->bond_water) * prsv->c_Qout;

    /* Calculating the number of layer being affected by reservoir drainage.
       In any case, count_layer is at least = 1. count_layer-1 are totally drained.
       count_layer is partially drained. */

    sum_h_layer = 0.;
    count_layer = 0;
    for (i = 0; i < prsv->nb_layer; i++) {
      sum_h_layer += prsv->p_layer[i]->height;
      count_layer++;
      if (sum_h_layer > q_grav)
        break;
    }

    //	LP_printf(fpout,"Nombre de couches a vidanger : %d\n",count_layer);

    // Calculating outflow
    sum_h_layer = 0.;

    qout = q_grav + q_surverse; // Water

    // Drained matter flux due to fully drained layers
    for (i = 0; i < count_layer - 1; i++) {
      for (p = 0; p < nb_species; p++)
        fout[p] += (prsv->p_layer[i]->transp_var[p] * prsv->p_layer[i]->height);

      sum_h_layer += prsv->p_layer[i]->height; // Water height due to fully drained layers
    }

    // Adding water due to partially drained layer
    q_remain = q_grav - sum_h_layer;

    for (p = 0; p < nb_species; p++)
      fout[p] += (prsv->p_layer[count_layer - 1]->transp_var[p] * q_remain);

    // Updating layer water height of the partially drained layer
    prsv->p_layer[count_layer - 1]->height -= q_remain;

    // Concentration of drained water flow
    if (qout > EPS_RSV) {
      for (p = 0; p < nb_species; p++)
        cout[p] = fout[p] / qout;
    }

    // Layer pile resizing
    prsv->z = RSV_resize_reservoir_layers(prsv, count_layer - 1, nb_species, fpout);

    // Balance updates fo the next time step
    // Water
    pwatbal->q[res_out] = qout;
    pwatbal->q[RSV_BIL] += prsv->z - z_init;
    // Matter
    for (p = 0; p < nb_species; p++) {
      pwatbal->trvar[p][res_out] = cout[p];
      pwatbal->trflux[p][res_out] = fout[p] * 1.e-3; // back to g/m2
      pwatbal->trflux[p][RSV_BIL] += (f_init[p] - pwatbal->trflux[p][res_out]);
    }
  } else // Storage due to bond fraction
  {
    pwatbal->q[res_out] = 0.;
    pwatbal->q[RSV_BIL] += prsv->z - z_init;
    for (p = 0; p < nb_species; p++) {
      pwatbal->trvar[p][res_out] = 0.;
      pwatbal->trflux[p][res_out] = 0.;
      pwatbal->trflux[p][RSV_BIL] += (f_init[p] - pwatbal->trflux[p][res_out]);
    }
  }

  free(fout);
  free(cout);
  free(f_init);
}

/**
 * @brief Prints current characteristics of a single layered reservoir
 */
void RSV_print_rsv_transport_properties(s_reservoir_rsv *prsv, int nb_species, FILE *fp) {

  int k, l;

  if (prsv == NULL) {
    LP_error(fp, "librsv%4.2f : Error in file %s, function %s at line %d : Bucket pointer NULL.\n", VERSION_RSV, __FILE__, __func__, __LINE__);
  }

  LP_printf(fp, "Current water level %f Height %f Nb_layer %d Bond_water %f C_Qout %f\n", prsv->z, prsv->height, prsv->nb_layer, prsv->bond_water, prsv->c_Qout);

  if (prsv->nb_layer > 0) {
    for (k = 0; k < prsv->nb_layer; k++) {
      LP_printf(fp, "Layer %d : Height %f\n", k, prsv->p_layer[k]->height);
      for (l = 0; l < nb_species; l++)
        LP_printf(fp, "Specie %d : Concentration %f\n", l, prsv->p_layer[k]->transp_var[l]);
    }
  } else {
    LP_printf(fp, "Reservoir with no layers.\n");
  }
}