/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: librsv
 * FILE NAME: struct_rsv.h
 *
 * CONTRIBUTORS: Nicolas FLIPO, Nicolas GALLOIS, Baptiste LABARTHE
 *
 * LIBRARY BRIEF DESCRIPTION: Simulation of a reservoir 0.5D.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the librsv Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        struct_rsv.h
 * @brief       RSV library structures
 * @author      Nicolas FLIPO, Nicolas GALLOIS, Baptiste LABARTHE
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

typedef struct reservoir s_reservoir_rsv;
typedef struct layer_reservoir s_layer_reservoir_rsv;
typedef struct water_balance s_wat_bal_rsv;

/**
 * @struct reservoir
 * @brief Reservoir structure attributes
 */
struct reservoir {
  /* Reservoir height */
  double height;
  /* Bound water height */
  double bond_water;
  /* Drainage coefficient */
  double c_Qout;
  /* Number of effective layers (i.e. actually containing water and transport species) */
  int nb_layer;
  /* Current layers buffer size. Evolves in multiples of BUFFER_NB_LAYER_RSV */
  int layer_buffersize;
  /* Current water height */
  double z;
  /* Reservoir soil elevation */ // Obsolete attribute.
  double surf;
  /* Array which contains transport variable of interest values (e.g., C, T) for each species. Sized over the number of species */
  double transp_var;
  /* Layer pointers array: position 0 corresponds to the reservoir bottom, and position (nb_layer-1) corresponds to the top layer */
  s_layer_reservoir_rsv **p_layer;
  /* Pointer to the next reservoir */
  s_reservoir_rsv *next;
  /* Pointer to the previous reservoir */
  s_reservoir_rsv *prev;
};

/**
 * @struct layer_reservoir
 * @brief Reservoir layer structure attributes
 */
struct layer_reservoir {
  /* Layer height */
  double height;
  /* Variable of interest values (e.g., C, T) for each species */
  double *transp_var;
  /* Pointer to the next layer */
  s_layer_reservoir_rsv *next;
  /* Pointer to the previous layer */
  s_layer_reservoir_rsv *prev;
};

/**
 * @struct water_balance
 * @brief Water balance structure attributes
 */
struct water_balance {
  /* Reservoir water levels: balance (BIL), runoff (RUIS), and infiltration (INFILT) */
  double r[RSV_NTYPE];
  /* Flows at the reservoir outlets: balance (BIL), runoff (RUIS), and infiltration (INFILT) */
  double q[RSV_NTYPE];
  /* Overflow from reservoirs */
  double ovf[RSV_NTYPE];
  /* Transport VoI (i.e. C, T) at the reservoir outlets: runoff (RUIS) and infiltration (INFILT) */
  double **trvar;
  /* Transport flux (i.e. matter, energy) at the reservoir outlets: runoff (RUIS) and infiltration (INFILT) */
  double **trflux;
};

#define new_reservoir() ((s_reservoir_rsv *)malloc(sizeof(s_reservoir_rsv)))
#define new_layer_reservoir() ((s_layer_reservoir_rsv *)malloc(sizeof(s_layer_reservoir_rsv)))
#define new_wat_bal() ((s_wat_bal_rsv *)malloc(sizeof(s_wat_bal_rsv)))