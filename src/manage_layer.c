/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: librsv
 * FILE NAME: manage_layer.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Nicolas GALLOIS, Baptiste LABARTHE
 *
 * LIBRARY BRIEF DESCRIPTION: Simulation of a reservoir 0.5D.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the librsv Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        manage_layer.c
 * @brief       Functions dealing with attributes of the s_layer_reservoir_rsv structure
 * @author      Nicolas FLIPO, Nicolas GALLOIS, Baptiste LABARTHE
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "reservoir.h"

/**
 * @brief Creates and initializes a new reservoir layer
 * @return New s_layer_reservoir_rsv* layer
 */
s_layer_reservoir_rsv *RSV_create_layer(int nb_species, FILE *fpout) {

  int j;
  s_layer_reservoir_rsv *player = NULL;

  player = new_layer_reservoir();
  bzero((char *)player, sizeof(s_layer_reservoir_rsv));

  player->transp_var = (double *)calloc(nb_species, sizeof(double)); // Set to 0.

  if (player->transp_var == NULL) {
    LP_error(fpout, "In libnrsv%4.2f : Error in file %s, in function %s at line %d : Memory allocation failure for concentrations array in new layer pointer.\n", VERSION_RSV, __FILE__, __func__, __LINE__);
  }

  // Default initialization
  player->height = 0.;

  return player;
}

/**
 * @brief Links two reservoir layers
 */
void RSV_chain_layer_fwd(s_layer_reservoir_rsv *pc1, s_layer_reservoir_rsv *pc2) {
  pc1->next = pc2;
  pc2->prev = pc1;
}

/**
 * @brief Links two reservoir layers
 */
void RSV_secured_chain_layer_fwd(s_layer_reservoir_rsv *pc1, s_layer_reservoir_rsv *pc2) {
  if (pc2 != NULL) {
    RSV_chain_layer_fwd(pc1, pc2);
  } else {
    pc1 = pc2;
  }
}

/**
 * @brief Initializes a reservoir layer pile and assign default settings to the reservoir
 * @internal NG : 14/04/2023 : In order to simplify layer memory management, a buffer of a given number of
 * reservoir layer is used (BUFFER_NB_LAYER_RSV). Only when about to reach buffer overflow, a new chunk of
 * BUFFER_NB_LAYER_RSV size is reallocated.
 * prsv->nb_layer is used to track is current number of layer which actually contains water and
 * transport specie
 * NG 22/11/2024 : At initialization, the water level imposed from input file fills the storage
 * capacity layer first. The remaining water goes in the layer above.
 */
void RSV_initialize_layers_transport(s_reservoir_rsv *prsv, int nb_species, double *trvar_init, FILE *fpout) {

  int i, j;

  RSV_init_new_chunk_layers(prsv, nb_species, fpout);

  // Default initialization procedure for water
  if (prsv->z <= prsv->bond_water) {
    prsv->nb_layer++;
    prsv->p_layer[prsv->nb_layer - 1]->height = prsv->z;
  } else {
    prsv->nb_layer++;
    prsv->p_layer[prsv->nb_layer - 1]->height = prsv->bond_water;
    prsv->nb_layer++; // Initial number of effective layers
    prsv->p_layer[prsv->nb_layer - 1]->height = prsv->z - prsv->bond_water;
  }

  // Concentration initialization
  for (i = 0; i < prsv->nb_layer; i++) {
    for (j = 0; j < nb_species; j++)
      prsv->p_layer[i]->transp_var[j] = trvar_init[j];
  }
  // LP_printf(fpout, "Number of initial effective layers : %d.\n", prsv->nb_layer); // NG check
}

/**
 * @brief Initializes by default transport attributes (i.e. water, transport species) for a chunk of
 * reservoir layers in case of buffer overflow. In case of realloc, the new portion of **p_layer is also initialized.
 */
void RSV_init_new_chunk_layers(s_reservoir_rsv *prsv, int nb_species, FILE *fpout) {

  int i, old_size;

  old_size = prsv->layer_buffersize;
  prsv->layer_buffersize += BUFFER_NB_LAYER_RSV;

  if (prsv->p_layer == NULL) {
    prsv->p_layer = ((s_layer_reservoir_rsv **)malloc(prsv->layer_buffersize * sizeof(s_layer_reservoir_rsv *)));
  } else {
    prsv->p_layer = (s_layer_reservoir_rsv **)realloc(prsv->p_layer, prsv->layer_buffersize * sizeof(s_layer_reservoir_rsv *));
  }

  if (prsv->p_layer == NULL) {
    LP_error(fpout, "librsv%4.2f : Error in file %s, in function %s at line %d : Memory (re)allocation failure for tabular of reservoir layer pointers.\n", VERSION_RSV, __FILE__, __func__, __LINE__);
  }

  // Creating and initializing layers of the newly allocated chunk
  for (i = old_size; i < prsv->layer_buffersize; i++) {
    prsv->p_layer[i] = RSV_create_layer(nb_species, fpout);
  }
  // LP_printf(fpout,"In function %s : Reservoir layer buffer size changed from %d to %d.\n", __func__, old_size,prsv->layer_buffersize); // NG check
}

/**
 * @brief Resets transport attribute values of a given reservoir layer
 */
void RSV_reset_layer_values(int nb_species, s_layer_reservoir_rsv *player, FILE *fp) {
  int j;

  if (player == NULL) {
    LP_error(fp, "In libnrsv%4.2f : Error in function %s at line %d : Trying to reset non-allocated layer reservoir pointer.\n", VERSION_RSV, __func__, __LINE__);
  }

  player->height = 0.;
  for (j = 0; j < nb_species; j++) {
    player->transp_var[j] = 0.;
  }
}

/**
 * @brief For all species, fills transport values in a pre-allocated and initialized reservoir layer
 */
void RSV_fill_top_layer(int nb_species, s_reservoir_rsv *prsv, s_wat_bal_rsv *pwatbal, int res_out) {
  int p, pos;

  prsv->nb_layer++;         // Effective number of layers update
  pos = prsv->nb_layer - 1; // Top position of the effective layer pile

  prsv->p_layer[pos]->height = pwatbal->q[res_out];

  for (p = 0; p < nb_species; p++)
    prsv->p_layer[pos]->transp_var[p] = pwatbal->trvar[p][res_out];

  // Update of the total water height of the reservoir (mm)
  prsv->z += pwatbal->q[res_out];
}

/**
 * @brief Dissolves the incoming reservoir layer at the beginning of a time step
 * (totally or partially) into the strage capacity layer. Updates the storage capacity
 * layer attributes (height, concentration) and updates the attributes of the dissolved layer.
 */
void RSV_dissolve_layer_into_storage_cap(int nb_species, s_reservoir_rsv *prsv, s_wat_bal_rsv *pwatbal, int res_out, FILE *flog) {

  int p;
  float h_diss, c_new, h_new;

  // Update of the total water height of the reservoir (mm)
  prsv->z += pwatbal->q[res_out];

  // LP_printf(flog, "Water input %f - Current total water height %f\n", pwatbal->q[res_out], prsv->z);

  if (prsv->z > prsv->bond_water) { // z is now > bond_water => partial layer dissolution into the storage capacity layer

    h_diss = prsv->z - prsv->bond_water;

    for (p = 0; p < nb_species; p++) {
      c_new = pwatbal->trvar[p][res_out];
      RSV_update_concentration_layer(p, 0, prsv, c_new, h_diss, res_out); // Updating the storage capacity layer concentration after the stacking of the new layer
    }
    prsv->p_layer[0]->height += h_diss;
    pwatbal->q[res_out] -= h_diss;

    // LP_printf(flog, "Partial dissolution : Current water height of the capacity layer : %f mm\n", prsv->p_layer[0]->height); // NG check

  } else { // Case of total dissolution

    h_new = pwatbal->q[res_out];

    for (p = 0; p < nb_species; p++) {
      c_new = pwatbal->trvar[p][res_out];
      RSV_update_concentration_layer(p, 0, prsv, c_new, h_new, res_out);
    }

    // Updating the storage capacity layer height
    prsv->p_layer[0]->height += pwatbal->q[res_out];
    // LP_printf(flog,"Full dissolution : Current water height of the capacity layer : %f mm\n",prsv->p_layer[0]->height); // NG check
  }
}

/**
 * @brief Updates a layer concentration after a water inflow at c_new concentration.
 */
void RSV_update_concentration_layer(int id_species, int id_layer, s_reservoir_rsv *prsv, double c_new, double h_new, int res_out) {

  float c_old, h_old;

  c_old = prsv->p_layer[id_layer]->transp_var[id_species];
  h_old = prsv->p_layer[id_layer]->height;

  // Updated layer concentration in g/m3
  prsv->p_layer[id_layer]->transp_var[id_species] = ((c_old * h_old) + (c_new * h_new)) / (h_old + h_new);
}

/**
 * @brief Frees a layer
 * @return If OK, returns NULL pointer
 */
s_layer_reservoir_rsv *RSV_free_layer(s_layer_reservoir_rsv *player, FILE *fpout) {

  if (player == NULL) {
    LP_error(fpout, "librsv%4.2f : Error in file %s, function %s at line %d : RSV layer pointer is NULL. Nothing to free.\n", VERSION_RSV, __FILE__, __func__, __LINE__);
  }

  free(player->transp_var);
  free(player);
  player = NULL;
  return player;
}

/**
 * @brief Updates the structure of a layered bucket after being drained.
 * Function simply performs a translation of the content of **p_layer
 * @return Returns the updated water level in the bucket at the end of the time step
 * @internal nblay = number of layers to be erased (= fully drained at the current time step)
 */
double RSV_resize_reservoir_layers(s_reservoir_rsv *prsv, int nblay, int nb_species, FILE *fpout) {

  int i, p, nblay_old = prsv->nb_layer;
  double wlev = 0.;

  // Translating attributes = not the best move, but simpler (no tricky pointer management)

  for (i = nblay; i < prsv->nb_layer; i++) {
    prsv->p_layer[i - nblay]->height = prsv->p_layer[i]->height;
    for (p = 0; p < nb_species; p++) {
      prsv->p_layer[i - nblay]->transp_var[p] = prsv->p_layer[i]->transp_var[p];
    }
  }

  // Update of the number of effective layers
  prsv->nb_layer -= nblay;

  // Remaining part of the buffer
  for (i = prsv->nb_layer; i < nblay_old; i++)
    RSV_reset_layer_values(nb_species, prsv->p_layer[i], fpout);

  for (i = 0; i < prsv->nb_layer; i++)
    wlev += prsv->p_layer[i]->height;

  // Refresh of the current total water level
  prsv->z = wlev;

  // Checking if an entire buffer can be erased.
  if (prsv->layer_buffersize - prsv->nb_layer > BUFFER_NB_LAYER_RSV) {

    for (i = prsv->layer_buffersize - BUFFER_NB_LAYER_RSV; i < prsv->layer_buffersize; i++)
      prsv->p_layer[i] = RSV_free_layer(prsv->p_layer[i], fpout);

    // Shrinking back the p_layer array from a while buffer size
    prsv->layer_buffersize -= BUFFER_NB_LAYER_RSV;
    prsv->p_layer = (s_layer_reservoir_rsv **)realloc(prsv->p_layer, prsv->layer_buffersize * sizeof(s_layer_reservoir_rsv *));
  }

  return prsv->z;
}