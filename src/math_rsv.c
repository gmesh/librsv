/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: librsv
 * FILE NAME: math_rsv.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Nicolas GALLOIS, Baptiste LABARTHE
 *
 * LIBRARY BRIEF DESCRIPTION: Simulation of a reservoir 0.5D.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the librsv Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        math_rsv.c
 * @brief       Conversion functions applied to units, input and output reservoir flows
 * @author      Nicolas FLIPO, Nicolas GALLOIS, Baptiste LABARTHE
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "reservoir.h"

/**
 * @brief Converts a water height in mm into a discharge in m3/s.
 * @return Discharge in m3/s
 */
double RSV_mm_to_q(double mm, double area, double dt, int unit_dt, FILE *fpout) {
  double q_out;

  // LP_printf(fpout,"in RSVmm2q : mm %f, area %f dt %f unit_dt %d\n",mm,area,dt,unit_dt); BL to debug
  q_out = (mm * 0.001 * area) / (dt * TS_unit2sec(unit_dt, fpout));
  // LP_printf(fpout,"qout : %f\n",q_out); BL to debug
  return q_out;
}

/**
 * @brief Converts a discharge in m3/s into a water height (associated with a given time step duration)
 * @return Water height in mm
 */
double RSV_q_to_mm(double flux, double area, double dt, int unit_dt, FILE *fpout) {
  double q_out = 0.;

  q_out = (flux * dt * TS_unit2sec(unit_dt, fpout)) / (0.001 * area);
  return q_out;
}

/**
 * @brief Converts water height in mm into a volume (associated with a given time step duration)
 * @return volume in m3
 */
double RSV_mm_to_volume(double mm, double area, double dt, int unit_dt, FILE *fpout) {
  double vol = 0.;

  vol = mm * 0.001 * area * dt * TS_unit2sec(unit_dt, fpout);
  return vol;
}

/**
 * @brief Converts a volume (associated with a given time step duration) into a water height
 * @return Water height in mm
 */
double RSV_volume_to_mm(double vol, double area, double dt, int unit_dt, FILE *fpout) {
  double qout = 0.;

  qout = (1000. * vol) / (dt * TS_unit2sec(unit_dt, fpout) * area);
  return qout;
}

/**
 * @brief Converts a discharge in m3/s into a volume (associated with a given time step duration)
 * @return Volume in m3
 */
double RSV_q_to_volume(double q, double dt, int unit_dt, FILE *fpout) {
  double vol = 0.;

  vol = q * dt * TS_unit2sec(unit_dt, fpout);
  return vol;
}

/**
 * @brief Converts a volume (associated with a given time step duration) into a discharge in m3/s
 * @return Discharge in m3/s
 */
double RSV_volume_to_q(double vol, double dt, int unit_dt, FILE *fpout) {
  double q = 0.;

  q = vol / (dt * TS_unit2sec(unit_dt, fpout));
  return q;
}