/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: librsv
 * FILE NAME: itos.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Nicolas GALLOIS, Baptiste LABARTHE
 *
 * LIBRARY BRIEF DESCRIPTION: Simulation of a reservoir 0.5D.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the librsv Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        itos.c
 * @brief       Integer-to-string conversion functions
 * @author      Nicolas FLIPO, Nicolas GALLOIS, Baptiste LABARTHE
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "reservoir.h"

/**
 * @brief Integer-to-string conversion function fo reversoir names
 * @return Reservoir name (as a string)
 */
char *RSV_name_res(int iname) {
  char *name;

  switch (iname) {
  case RSV_RUIS: {
    name = strdup("RUISS");
    break;
  }
  case RSV_SOUT: {
    name = strdup("FROM_AQ");
    break;
  }
  case RSV_NORIGINE: {
    name = strdup("NORIGINE");
    break;
  }
  case RSV_BIL: {
    name = strdup("WATER");
    break;
  }
  case RSV_INFILT: {
    name = strdup("INFILT");
    break;
  }
  case RSV_ETR: {
    name = strdup("ETR");
    break;
  }
  case RSV_DIRECT: {
    name = strdup("DIRECT_SOUT");
    break;
  }
  default: {
    name = strdup("Unknown reservoir type");
    break;
  }
  }
  return name;
}