/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: librsv
 * FILE NAME: manage_wat_bal.c
 *
 * CONTRIBUTORS: Nicolas FLIPO, Nicolas GALLOIS, Baptiste LABARTHE
 *
 * LIBRARY BRIEF DESCRIPTION: Simulation of a reservoir 0.5D.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the librsv Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        manage_wat_bal.c
 * @brief       Functions dealing with attributes of the s_wat_bal_rsv structure
 * @author      Nicolas FLIPO, Nicolas GALLOIS, Baptiste LABARTHE
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "reservoir.h"

/**
 * @brief Allocates and initializes a wat_bal structure
 * @return Newly allocated and set wat_bal structure
 */
s_wat_bal_rsv *RSV_create_wat_bal() {
  int i;
  s_wat_bal_rsv *pwb;

  pwb = new_wat_bal();
  bzero((char *)pwb, sizeof(s_wat_bal_rsv));

  for (i = 0; i < RSV_NTYPE; i++) {
    pwb->q[i] = 0.;
    pwb->r[i] = 0.;
    pwb->ovf[i] = 0.;
    pwb->trvar = NULL;
    pwb->trflux = NULL;
  }

  return pwb;
}

/**
 * @brief Resets watbal values for all reservoirs types
 */
void RSV_reinit_wat_bal(s_wat_bal_rsv *pwb) {
  int i;

  for (i = 0; i < RSV_NTYPE; i++) {
    pwb->q[i] = 0.;
    pwb->r[i] = 0.;
    pwb->ovf[i] = 0.;
  }
}

/**
 * @brief Re-initializes transport components (f,c) for all buckets,
 * for one single specie.
 */
void RSV_reinit_matter_fluxes_bal(s_wat_bal_rsv *pwb, int id_species) {
  int i;

  for (i = 0; i < RSV_NTYPE; i++) {
    pwb->trflux[id_species][i] = 0;
    pwb->trvar[id_species][i] = 0;
  }
}

s_wat_bal_rsv *RSV_free_wat_bal(s_wat_bal_rsv *pwb) {
  free(pwb);
  return NULL;
}

/**
 * @brief Allocates and initializes trflux and trvar attributes if transport
 * is activated for the total amount of species declared.
 */
void RSV_manage_memory_transport(s_wat_bal_rsv *pwatbal, int nb_species, FILE *flog) {

  int s, r;

  if (pwatbal->trflux != NULL) {
    LP_error(flog, "librsv%4.2f : Error in file %s, function %s at line %d. Memory has already been allocated for surface transport !?\n", VERSION_RSV, __FILE__, __func__, __LINE__);
  }

  pwatbal->trflux = (double **)malloc(nb_species * sizeof(double *));
  pwatbal->trvar = (double **)malloc(nb_species * sizeof(double *));

  if (pwatbal->trflux == NULL || pwatbal->trvar == NULL) {
    LP_error(flog, "librsv%4.2f, File %s in %s line %d : Bad memory management while allocating RSV pointers for transport.\n", VERSION_RSV, __FILE__, __func__, __LINE__);
  }

  bzero(pwatbal->trflux, nb_species * sizeof(double *));
  bzero(pwatbal->trvar, nb_species * sizeof(double *));

  for (s = 0; s < nb_species; s++) {
    pwatbal->trflux[s] = (double *)malloc(RSV_NTYPE * sizeof(double));
    pwatbal->trvar[s] = (double *)malloc(RSV_NTYPE * sizeof(double));

    for (r = 0; r < RSV_NTYPE; r++) {
      pwatbal->trflux[s][r] = 0.;
      pwatbal->trvar[s][r] = 0.;
    }
  }
}