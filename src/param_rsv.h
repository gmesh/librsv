/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: librsv
 * FILE NAME: param_rsv.h
 *
 * CONTRIBUTORS: Nicolas FLIPO, Nicolas GALLOIS, Baptiste LABARTHE
 *
 * LIBRARY BRIEF DESCRIPTION: Simulation of a reservoir 0.5D.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the librsv Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        param_rsv.h
 * @brief       librsv parameters and enumerations
 * @author      Nicolas FLIPO, Nicolas GALLOIS, Baptiste LABARTHE
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

#define YES YES_TS
#define NO NO_TS

/**
 * @def EPS_RSV
 * @brief Default epsilon threshold value
 */
#define EPS_RSV EPS_TS

/**
 * @def BUFFER_NB_LAYER_RSV
 * @brief Default buffer chunk of number of reservoir layers
 */
#define BUFFER_NB_LAYER_RSV 50

/**
 * @def NBLAYER_INI_DEFAULT_RSV
 * @brief Default number of layers to receive initial reservoir settings
 */
#define NBLAYER_INI_DEFAULT_RSV 2

/**
 * @def CODE_RSV
 * @brief Default (error) code value
 */
#define CODE_RSV CODE_TS

/**
 * @def VERSION_RSV
 * @brief Library version ID
 */
#define VERSION_RSV 0.13

/**
 * @enum type_niveau
 * @brief List of reservoir types
 * @internal RSV_NORIGINE seems useless. Not called anywhere.
 */
enum type_niveau { RSV_RUIS, RSV_SOUT, RSV_NORIGINE, RSV_BIL, RSV_INFILT, RSV_ETR, RSV_DIRECT, RSV_NTYPE };