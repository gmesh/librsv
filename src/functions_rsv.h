/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: librsv
 * FILE NAME: functions_rsv.h
 *
 * CONTRIBUTORS: Nicolas FLIPO, Nicolas GALLOIS, Baptiste LABARTHE
 *
 * LIBRARY BRIEF DESCRIPTION: Simulation of a reservoir 0.5D.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the librsv Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/**
 * @file        functions_rsv.h
 * @brief       RSV library functions
 * @author      Nicolas FLIPO, Nicolas GALLOIS, Baptiste LABARTHE
 * @date        2024
 * @copyright   Eclipse Public License - v 2.0
 */

// in manage_rsv.c
s_reservoir_rsv *RSV_free_rsv(s_reservoir_rsv *);
s_reservoir_rsv *RSV_chain_rsv_fwd(s_reservoir_rsv *, s_reservoir_rsv *);
s_reservoir_rsv *RSV_secured_chain_rsv_fwd(s_reservoir_rsv *, s_reservoir_rsv *);
s_reservoir_rsv *RSV_create_reservoir(double, double, double);
double RSV_transit_simple(s_wat_bal_rsv *, double, double, double, int, s_reservoir_rsv *, int, int, FILE *); // NG : 05/12/2023 : Adding overflow rerouting option and destination bucket as arguments.
// ***** Transport functions *****
void RSV_transit_layered_reservoir(int, int, int, s_reservoir_rsv *, s_wat_bal_rsv *, int, FILE *);
void RSV_print_rsv_transport_properties(s_reservoir_rsv *, int, FILE *);

// in manage_layer.c
s_layer_reservoir_rsv *RSV_create_layer(int, FILE *);
void RSV_chain_layer_fwd(s_layer_reservoir_rsv *, s_layer_reservoir_rsv *);
void RSV_secured_chain_layer_fwd(s_layer_reservoir_rsv *, s_layer_reservoir_rsv *);
s_layer_reservoir_rsv *RSV_free_layer(s_layer_reservoir_rsv *, FILE *);
// ***** Transport functions *****
void RSV_initialize_layers_transport(s_reservoir_rsv *, int, double *, FILE *);
void RSV_init_new_chunk_layers(s_reservoir_rsv *, int, FILE *);                                 // NG : 14/04/2023
void RSV_fill_top_layer(int, s_reservoir_rsv *, s_wat_bal_rsv *, int);                          // NG : 14/04/2023
void RSV_dissolve_layer_into_storage_cap(int, s_reservoir_rsv *, s_wat_bal_rsv *, int, FILE *); // NG : 21/11/2024
void RSV_update_concentration_layer(int, int, s_reservoir_rsv *, double, double, int);          // NG : 21/11/2024
void RSV_reset_layer_values(int, s_layer_reservoir_rsv *, FILE *);                              // NG : 14/04/2023
double RSV_resize_reservoir_layers(s_reservoir_rsv *, int, int, FILE *);                        // NG : 14/04/2023

// in manage_wat_bal.c
s_wat_bal_rsv *RSV_create_wat_bal();
void RSV_reinit_wat_bal();
s_wat_bal_rsv *RSV_free_wat_bal(s_wat_bal_rsv *);
// ***** Transport functions *****
void RSV_manage_memory_transport(s_wat_bal_rsv *, int, FILE *);
void RSV_reinit_matter_fluxes_bal(s_wat_bal_rsv *, int);

// in maths_rsv.c
double RSV_mm_to_q(double, double, double, int, FILE *);
double RSV_q_to_mm(double, double, double, int, FILE *);
double RSV_mm_to_volume(double, double, double, int, FILE *);
double RSV_volume_to_mm(double, double, double, int, FILE *);
double RSV_q_to_volume(double, double, int, FILE *);
double RSV_volume_to_q(double, double, int, FILE *);

// in itos.c
char *RSV_name_res(int);